package me.fwfurtado.apigateway;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompositionController {

    @GetMapping("composition")
    public ResponseEntity<?> compose() {
        // RestTemplate

        return ResponseEntity.ok().build();
    }

}
